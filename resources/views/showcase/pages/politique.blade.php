@extends('layouts.template')
@section('content')
<section class="bloc">
  <section class="bloc-premier md:mt-8 md:flex md:flex-row md:justify-items-center md:justify-center">
    <section class="w-full mt-16 flex flex-col gap-16">
      <article class="flex flex-col gap-5">
        <article class="font-bold text-blue-900 text-4xl"><h1>Politique de <span class="text-yellow-600">confidentialité </span>et de <span class="text-yellow-600">traitement des données </span></h1></article>
      </article>
      <article class="flex flex-row gap-16">
        <politique-component></politique-component>
      </article>
    </section>
  </section>
</section>
@stop