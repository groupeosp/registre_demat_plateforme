<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermanencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permanences', function (Blueprint $table) {
            $table->increments('per_id');
            $table->integer('reg_id');
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->tinyInteger('per_type');
            $table->integer('per_lieu');
            $table->dateTime('per_debut');
            $table->dateTime('per_fin');
            $table->tinyInteger('per_interval');
            $table->boolean('per_sans_rdv');
            $table->string('per_url_visio', 120);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permanences');
    }
}
