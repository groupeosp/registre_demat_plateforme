<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationPersoChampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observation_perso_champs', function (Blueprint $table) {
            $table->increments('opc_id');
            $table->integer('reg_id');
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->string('opc_nom', 120);
            $table->string('opc_type', 8);
            $table->boolean('opc_obligatoire');
            $table->boolean('opc_public');
            $table->text('opc_choix1');
            $table->text('opc_choix2');
            $table->text('opc_choix3');
            $table->text('opc_choix4');
            $table->text('opc_choix5');
            $table->text('opc_choix6');
            $table->text('opc_choix7');
            $table->text('opc_choix8');
            $table->text('opc_choix9');
            $table->text('opc_choix10');
            $table->integer('opc_position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observation_perso_champs');
    }
}
