<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLieuConsultationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lieu_consultation', function (Blueprint $table) {
            $table->increments('lie_id');
            $table->integer('reg_id')->unsigned();
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->text('lie_lieu');
            $table->text('lie_adresse');
            $table->text('lie_infos');
            $table->string('lie_departement', 80);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lieu_consultation');
    }
}
