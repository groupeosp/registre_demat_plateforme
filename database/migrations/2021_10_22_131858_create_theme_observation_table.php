<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemeObservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theme_observation', function (Blueprint $table) {
            $table->integer('the_id');
            $table->foreign('the_id')->references('the_id')->on('theme')->onDelete('cascade');
            $table->integer('obs_id');
            $table->foreign('obs_id')->references('obs_id')->on('observation')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theme_observation');
    }
}
