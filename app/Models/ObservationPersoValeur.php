<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ObservationPersoValeur extends Model
{
    protected $table = 'observation_perso_valeurs';
}
