<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ObservationPersoChamp extends Model
{
    protected $table = 'observation_perso_champs';
}
