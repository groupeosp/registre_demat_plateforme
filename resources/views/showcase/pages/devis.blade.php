@extends('layouts.template')
@section('content')
<section class="bloc">
  <section class="bloc-premier md:mt-8 md:flex md:flex-row md:justify-items-center md:justify-center">
    <section class="w-full mt-16 flex flex-col gap-16">
      <article class="flex flex-col gap-5">
        <article class="font-bold text-blue-900 text-4xl"><h1>À chaque besoin, sa solution<span class="text-yellow-500"> registredemat.fr<span class="inline text-blue-900">!</span>  </span></h1></article>
        <article class="text-2xl"><p>Quelle est la vôtre ?</p></article>
      </article>
      <article class="flex flex-row gap-16">
        <devis-form></devis-form>
      </article>
    </section>
  </section>
</section>
@stop