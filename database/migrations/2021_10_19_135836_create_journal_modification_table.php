<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalModificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_modification', function (Blueprint $table) {
            $table->increments('jou_id');
            $table->integer('reg_id')->unsigned();
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->dateTime('jou_date');
            $table->text('jou_action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_modification');
    }
}
