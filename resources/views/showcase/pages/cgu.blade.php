@extends('layouts.template')
@section('content')
<section class="bg-blue-100 pt-4 pb-4 ">
    <div class="bloc-premier rounded-xl shadow-lg bg-white p-16 textcgv">
        <h2>PREAMBULE</h2>
        Les présentes conditions générales d’utilisation ont pour objet de définir les modalités de mise à disposition et les conditions d’utilisation des services du site  www.registredemat.fr  par l'utilisateur.
        <br />
        <br />
        Tout accès au site www.registredemat.fr suppose l'acceptation et le respect de l'ensemble des termes des présentes conditions générales d’utilisation.
        <br />
        <br />
        www.registredemat.fr se réserve le droit de modifier le contenu des présentes conditions générales d’utilisation.
        <br />
        <br />
        <h2>DEFINITIONS</h2>
        Utilisateur : « l'utilisateur » est toute personne qui utilise l’un des services proposé par le site  www.registredemat.fr.
        Identifiant : le terme « Identifiant » recouvre les informations nécessaires à l'identification d'un utilisateur sur le site pour accéder aux zones réservées aux membres.
        Code d’accès : le « Code d’accès » est une information confidentielle permettant à l’utilisateur, conjointement avec son identifiant, de prouver son identité.
        <br />
        <br />
        <h2>ACCES AU SERVICE</h2>
        Le site  www.registredemat.fr est accessible à toute personne à laquelle la société LEGALCOM a délivré un identifiant et un code d’accès.
        <br />
        <br />
        LEGALCOM se réserve le droit, unilatéralement, de :
        <br />
        <br />
        -	ne pas donner suite à une demande d’accès utilisateur au site www.registredemat.fr ;
        <br />
        <br />
        -	refuser l'accès au site www.registredemat.fr, sans notification préalable, à tout utilisateur ne respectant pas les présentes conditions d'utilisation ;
        <br />
        <br />
        -	interrompre, suspendre ou modifier sans préavis l'accès à tout ou partie du site  www.registredemat.fr, afin d'en assurer les maintenances et mises à jour, sans que l'interruption n'ouvre droit à aucune obligation ni indemnisation.
        <br />
        <br />
        LEGALCOM s’engage à fournir un accès optimal au logiciel 24 heures sur 24, 7 jours sur 7. Cette obligation étant une obligation de moyens.
        <br />
        <br />
        LEGALCOM ne pourra être tenu responsable des dommages subis par le client suite à l'indisponibilité temporaire des services rendue indispensable par les opérations de maintenance et de mise à jour des logiciels.
        <br />
        <br />
        L’utilisateur reconnaît avoir vérifié l'adéquation du service à ses besoins et notamment l'équipement et la connexion Internet nécessaires à l'utilisation des services proposés par www.registredemat.fr.
        <br />
        <br />
        L’utilisateur s'engage à respecter l'ensemble des réglementations en vigueur, et en particulier celles relatives à la déclaration des traitements des fichiers de données à caractère personnel auprès de la Commission Nationale de l'Informatique et des Libertés (C.N.I.L.).
        SECURITE
        Les identifiant et code d’accès fournis à l’utilisateur sont strictement confidentiels. L’utilisateur s'engage à ne pas les divulguer à des tiers, leur usage se fait sous son entière responsabilité.
        <br />
        <br />
        LEGALCOM s'engage à prendre toute précaution raisonnable pour assurer la protection matérielle des données et programmes.
        <br />
        <br />
        LEGALCOM se réserve le droit de refuser la présence sur ses serveurs, de fichiers importés par l’utilisateur qui seraient jugés techniquement non conformes ou nuisibles à ces derniers.
        <br />
        <br />
        <h2>CONFIDENTIALITE</h2>
        Les informations personnelles relatives à l’utilisateur (données société, activité, coordonnées, etc…) sont exclusivement réservées à la gestion des relations commerciales entre LEGALCOM et l’utilisateur/client.
        <br />
        <br />
        LEGALCOM s'engage à garder les données de l’utilisateur confidentielles, à n'en effectuer, en dehors des nécessités techniques, aucune copie et à n'en faire aucune utilisation autre que celles prévues pour l'exécution de la prestation.
        <br />
        <br />
        <h2>PROPRIETE INTELLECTUELLE</h2>
        Les éléments accessibles sur le site www.registredemat.fr tels que les logiciels, bases de données, textes, images, photographies, et plus généralement l'ensemble des informations mises à la disposition du client dans le cadre du contrat, sont la propriété exclusive de LEGALCOM.
        L’utilisateur ne peut, en aucun cas, reproduire, modifier ou exploiter de quelque manière que ce soit, ces éléments sans l'autorisation écrite préalable de LEGALCOM.
        <br />
        <br />
        <h2>PROTECTION DES DONNEES A CARACTERE PERSONNEL</h2>
        Conformément à la loi informatique et libertés du 6 janvier 1978, l’utilisateur dispose d'un droit d'accès, de rectification et d'opposition aux données personnelles le concernant.
        <br />
        <br />
        Pour exercer ce droit, il suffit à l’utilisateur d'en faire la demande en indiquant ses nom, prénom, adresse et si possible sa référence client à l’adresse postale : LEGALCOM - 14 rue Beffroy - CS 30018 - 92523 Neuilly-sur-Seine Cedex.
        <br />
        <br />
        <h2>DROIT APPLICABLE – LITIGES</h2>
        Les présentes conditions générales d’utilisation sont régies par la loi française.
        Tous litiges relatifs à l'interprétation ou à l'exécution des présentes conditions générales d’utilisation seront soumis à la compétence du Tribunal de Commerce de Nanterre.
    </div>
    </section>
@stop



