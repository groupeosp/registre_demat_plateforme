@extends('layouts.template')
@section('content')
<section class="bloc">
  <section class="bloc-premier md:mt-8 md:flex md:flex-row md:justify-items-center md:justify-center">
    <section class="w-full mt-16 flex flex-col gap-16">
        <article class="flex flex-col gap-5">
            <article class="font-bold text-blue-900 text-4xl"><h1>Glossaire</h1></article>
        </article>
        <article class="flex flex-row gap-16">
            <div class="container">
                <!-- Glossaire div starts -->
                <div class="animated bg-white fadeInDown flex-wrap mb-16 top-0 rounded-2xl shadow-lg p-4 w-full wow">
                    <div class="bg-transparent border-2 rounded-lg">
                        <!-- AOE div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">AOE :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Autorité Organisatrice de l'Enquête.</p>
                        </div>
                        <!-- AOE div ends -->
                        <!-- Adware div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Adware :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Logiciel qui contient une publicité et surgit souvent de manière intempestive.</p>
                        </div>
                        <!-- Adware div starts -->
                        <!-- Anti-spam div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Anti-spam :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Logiciel qui permet de se prémunir contre le spam afin de limiter le volume de courriels non sollicités.</p>
                        </div>
                        <!-- Anti-spam div starts -->
                        <!-- Back-office div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Back-office :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Il s’agit de la partie non visible d’un site internet, qui permet de gérer le contenu et les fonctionnalités accessible par un nom d’utilisateur et un mot de passe.</p>
                        </div>
                        <!-- Back-office div starts -->
                        <!-- CAPTCHA div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">CAPTCHA :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">marque commerciale désignant.</p>
                        </div>
                        <!-- CAPTCHA div starts -->
                        <!-- CE div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">CE :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Commissaire enquêteur.</p>
                        </div>
                        <!-- CE div starts -->
                        <!-- Chat div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Chat :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Service disponible sur internet permettant la discussion à plusieurs.</p>
                        </div>
                        <!-- Chat div starts -->
                        <!-- CNCE div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">CNCE :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Compagnie Nationale des Commissaires Enquêteurs.</p>
                        </div>
                        <!-- CNCE div starts -->
                        <!-- CNDP div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">CNDP :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Commission Nationale du Débat Public.</p>
                        </div>
                        <!-- CNDP div starts -->
                        <!-- CNIL div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">CNIL :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Commission Nationale de l'Informatique et des Libertés.</p>
                        </div>
                        <!-- CNIL div starts -->
                        <!-- Code QR div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Code QR :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Type de code barres permettant d'être scanné pour envoyer sur le lien URL souhaité.</p>
                        </div>
                        <!-- Code QR div starts -->
                        <!-- Comissaire enquêteur div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Comissaire enquêteur :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">est la personne nommée par le Président du Tribunal Administratif lors d'une commission d'aptitude pour mener une enquête publique. Sa première mission est de s'assurer de la publicité en vérifiant que l'annonce légale soit parue dans la presse locale, que l'ouverture de l'enquête et ses modalités soient affichées en mairie et sur le lieu précis du projet d'aménagement. Il vérifie également que le dossier est disponible en mairie pour la consultation aux heures d'ouverture pour le public. Il gère les courriers qu'il reçoit et les insère au registre d'enquête ; effectue des permanences en mairie pour recevoir le public et mettre à sa disposition le registre d'enquête. Pour conclure l'enquête, il rédige un rapport dans le mois suivant cette fermeture puis dans un autre rapport, il rédige son avis personnel sur le bien fondé du projet.</p>
                        </div>
                        <!-- Comissaire enquêteur div starts -->
                        <!-- Cookie div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Cookie :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Fichier écrit sur l'ordinateur de l'internaute par le serveur web distant, permettant de sauvegarder des données de connexion (produits commandés, préférences...)</p>
                        </div>
                        <!-- Cookie div starts -->
                        <!-- ENE div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">ENE :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Loi Engagement National pour l'Environnement</p>
                        </div>
                        <!-- ENE div starts -->
                        <!-- FAQ div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">FAQ :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Foire aux questions.</p>
                        </div>
                        <!-- FAQ div starts -->
                        <!-- Modérateur div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Modérateur :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Personne chargée de l'animation et la bonne tenue des forums.</p>
                        </div>
                        <!-- Modérateur div starts -->
                        <!-- MOE div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">MOE :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Maître d'œuvre. </p>
                        </div>
                        <!-- MOE div starts -->
                        <!-- MOA div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">MOA :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Maître d'ouvrage. </p>
                        </div>
                        <!-- MOA div starts -->
                        <!-- RGAA div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">RGAA :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Référenciel Général d'Accessibilité des Administrations.</p>
                        </div>
                        <!-- RGAA div starts -->
                        <!-- Robot div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">Robot :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">aussi appelé « bot » informatique est un agent logiciel qui interagit comme le ferait un humain.</p>
                        </div>
                        <!-- Robot div starts -->
                        <!-- SPAM div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">SPAM :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">envoi massif et automatique de courriers électroniques non sollicités.</p>
                        </div>
                        <!-- SPAM div starts -->
                        <!-- SPYWARE div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">SPYWARE :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">logiciel espion destiné à collecter des informations sur les habitudes de navigation d'un utilisateur.</p>
                        </div>
                        <!-- SPYWARE div starts -->
                        <!-- TA div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">TA :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Tribunal Administratif</p>
                        </div>
                        <!-- TA div starts -->
                        <!-- URL div starts -->
                        <div class="p-1 text-justify">
                            <h2 class="mt-4 font-bold inline text-base md:text-lg lg:text-lg sm:text-lg text-yellow-600 wow fadeInUp animated">URL :</h2>
                            <p data-wow-delay="0.6s" class="fadeInUp inline leading-6 mb-1 text-gray-900 text-base wow">Adresse internet permettant d'identifier une page de site web. (Uniform Ressource Locator).</p>
                        </div>
                        <!-- URL div starts -->
                    </div>
                </div>
                <!-- Glossaire div ends -->
                <!-- help div starts -->
                <div class="text-center justify-center m-8 space-y-8">
                    <h2 class="mt-4 bg-blue-900 font-bold text-lg text-white wow fadeInUp animated">Besoin d'un conseil </h2>
                    <p data-wow-delay="0.6s" class="mb-1 text-gray-600 leading-6 text-base wow fadeInUp">Votre projet nécessite une enquête publique et vous souhaitez profiter de cette occasion pour communiquer. Appelez-nous, prenons rendez-vous! Notre équipe saura vous conseiller pour offrir une solution adaptée à vos besoins. </p>
                    <div class="text-center mb-1 wow fadeInUp" data-wow-delay="1.2s">
                        <a href="/contact" target="blank" rel="nofollow" class=" rounded-full font-bold px-3 py-2 bg-blue-900 text-white">Nous contacter</a>
                    </div>
                </div>
                <!-- help div ends -->
            </div>
        </article>
    </section>
  </section>
</section>
@stop