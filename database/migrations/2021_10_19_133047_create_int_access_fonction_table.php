<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntAccessFonctionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('int_access_fonction', function (Blueprint $table) {
            $table->integer('int_id')->unsigned();
            $table->foreign('int_id')->references('int_id')->on('intervenant')->onDelete('cascade');
            $table->integer('reg_id')->unsigned();
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->integer('int_acc_fonction');
            $table->tinyInteger('int_acc_previsu');
            $table->boolean('int_acc_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('int_access_fonction');
    }
}
