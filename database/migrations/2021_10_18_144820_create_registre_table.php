<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registre', function (Blueprint $table) {
            $table->increments('reg_id');
            $table->tinyInteger('reg_type');
            $table->string('reg_titre', 512);
            $table->string('reg_entete', 60);
            $table->string('reg_logo', 60);
            $table->text('reg_presentation');
            $table->string('reg_presentation_photo', 60);
            $table->string('reg_presentation_video', 120);
            $table->date('reg_ouverture');
            $table->date('reg_fermeture');
            $table->date('reg_fermeture_initiale');
            $table->date('reg_date_archivage');
            $table->string('reg_fuseau_horaire', 60);
            $table->tinyInteger('reg_integralite_dossier');
            $table->text('reg_objet');
            $table->string('reg_num_arrete', 100);
            $table->string('reg_date_arrete', 80);
            $table->string('reg_signataire_arrete', 255);
            $table->string('reg_num_ta', 80);
            $table->string('reg_arrete_pdf', 60);
            $table->integer('reg_arrete_pdf_count_download');
            $table->integer('reg_arrete_pdf_count_view');
            $table->string('reg_avis_enquete_pdf', 60);
            $table->integer('reg_avis_enquete_pdf_count_download');
            $table->integer('reg_avis_enquete_pdf_count_view');
            $table->string('reg_arrete_prolongation_pdf', 60);
            $table->integer('reg_arrete_prolongation_pdf_count_download');
            $table->integer('reg_arrete_prolongation_pdf_count_view');
            $table->string('reg_avis_prolongation_pdf', 60);
            $table->integer('reg_avis_prolongation_pdf_count_download');
            $table->integer('reg_avis_prolongation_pdf_count_view');
            $table->string('reg_ce_president', 120);
            $table->text('reg_ce_titulaire');
            $table->text('reg_ce_suppleant');
            $table->text('reg_siege_lieu');
            $table->text('reg_siege_adresse');
            $table->text('reg_siege_infos');
            $table->integer('reg_offre');
            $table->string('reg_url_interne', 80);
            $table->string('reg_url_externe', 80);
            $table->tinyInteger('reg_doc_interne');
            $table->string('reg_doc_url', 511);
            $table->text('reg_doc_presentation');
            $table->tinyInteger('reg_maintenance');
            $table->tinyInteger('reg_moderation');
            $table->tinyInteger('reg_telecharger');
            $table->tinyInteger('reg_visionner');
            $table->tinyInteger('reg_liseuse');
            $table->tinyInteger('reg_rdv');
            $table->tinyInteger('reg_rdv_auto');
            $table->tinyInteger('reg_visio');
            $table->tinyInteger('reg_reseaux_sociaux');
            $table->tinyInteger('reg_validation_mail');
            $table->tinyInteger('reg_mode_anonyme');
            $table->integer('reg_temps_tolere');
            $table->integer('reg_multiple');
            $table->tinyInteger('reg_visu_pre_eao');
            $table->tinyInteger('reg_visu_ep_eao');
            $table->tinyInteger('reg_visu_ep_c');
            $table->tinyInteger('reg_visu_docs_eao');
            $table->tinyInteger('reg_visu_docs_c');
            $table->tinyInteger('reg_visu_obs_o');
            $table->tinyInteger('reg_visu_obs_c');
            $table->tinyInteger('reg_email_dedie');
            $table->string('reg_email', 120);
            $table->string('reg_email_id', 120);
            $table->string('reg_email_pass', 120);
            $table->text('reg_email_hors_delais');
            $table->string('reg_css_perso', 60);
            $table->tinyInteger('reg_fiche');
            $table->string('reg_fiche_key', 60);
            $table->date('reg_cree_le');
            $table->string('reg_cree_par', 3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registre');
    }
}
