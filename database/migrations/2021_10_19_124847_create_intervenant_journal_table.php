<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntervenantJournalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intervenant_journal', function (Blueprint $table) {
            $table->increments('inj_id');
            $table->integer('int_id')->unsigned();
            $table->foreign('int_id')->references('int_id')->on('intervenant')->onDelete('cascade');
            $table->string('inj_nature', 60);
            $table->dateTime('inj_date_connexion');
            $table->dateTime('inj_date_der_activite');
            $table->dateTime('inj_date_deconnexion');
            $table->string('inj_ip', 15);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intervenant_journal');
    }
}
