@extends('layouts.template')
@section('content')
{{-- @foreach ($registres as $registre) @if ($registre->imgboolean)
            <img
                src="/storage/{{$registre->reg_id}}/real{{$registre->reg_id}}.jpg"
            />
            @else
            <img src="/images/realdemo.png" />
@endif @endforeach --}}

<section class="bloc">
  <section class="bloc-premier ">
  <section class="items-start mt-16 md:flex md:flex-row md:justify-items-center md:justify-center">
    <section class="bloc-devis text-center sm:w-1/3 flex flex-col gap-16">
      <article class="bloc-devis__video flex flex-col gap-5">
        <article class="bloc-devis__titre text-xl md:text-left lg:text-left sm:text-xl md:text-xl lg:text-4xl font-bold text-blue-900"><h1>La solution <span class="text-yellow-500">rapide <span class="text-blue-900">et </span>sécurisée </span>pour créer simplement votre <br /> <span class="text-yellow-500">registre dématerialisé en ligne</span></h1></article>
        <article class="bloc-devis__description text-lg md:text-left lg:text-left sm:text-2xl md:text-2xl lg:text-2xl"><p>Grâce à une experience utilisateur fluide et intuitive, Registredemat.fr met le registre dématerialisé à la portée de toutes les collectivités au travers d’un outil moderne et simple d’utilisation... à faible coût !</p></article>
      </article>
    <article class="bloc-devis__buttons flex items-center flex-col md:flex-row md:justify-center gap-16">
      <button class="btn_blue"><a href="{{ route('devis') }}">Devis</a></button>
      <div class="flex flex-row items-center gap-8">
        <playvideo-component></playvideo-component>
        <label for="bloc-devis__buttonplay" class="text-gray-400">Vidéo présentation</label>
      </div>
    </article>
  </section>
  <section class="bloc-image w-full sm:w-2/3">
    <img src="/images/home/montage.png" />
  </section>
</section></section>
<section class="bloc-premier mb-60">
  <div class="flex flex-col items-center md:flex-row md:justify-center gap-16">
    <div class="relative py-3">
      <div
        class="absolute inset-0 bg-gradient-to-r from-blue-100 to-blue-300 shadow-lg transform -skew-y-6 md:skew-y-0 md:-rotate-6 rounded-3xl">
      </div>
      <div class="relative bg-white shadow-lg rounded-3xl">
        <div class="bloc-image2 max-w-md">
          <img class="rounded-3xl" src="/images/home/legalimg.jpg" />
        </div>
      </div>
    </div>
    <div class="flex flex-col gap-8">
      <article class="md:text-left lg:text-left text-center font-bold text-blue-900 text-4xl"><h1>Legalcom</h1></article>
      <article class="textlegalcom1 md:text-left lg:text-left text-center text-lg sm:text-2xl md:text-2xl lg:text-2xl"><p>Filiale IT du groupe OSP, LEGALCOM est <br /> votre consultant spécialisé en <br /> communication légale, judiciaire, et <br /> formalités juridiques. <br /> LEGALCOM met aujourd'hui à votre <br /> disposition une forme d'accès moderne à <br /> la gestion de vos enquêtes publiques et <br /> cela avec des registres électroniques</p></article>
      <article class="textlegalcom2 md:text-left lg:text-left text-center text-lg sm:text-2xl md:text-2xl lg:text-2xl"><p>Filiale IT du groupe OSP, LEGALCOM est votre consultant spécialisé en communication légale, judiciaire, et formalités juridiques. LEGALCOM met aujourd'hui à votre disposition une forme d'accès moderne à la gestion de vos enquêtes publiques et cela avec des registres électroniques</p></article>
    </div>
  </div>
</section>
<section class="mt-32 sm:mt-0 md:mt-0 lg:mt-0">
  <tricardoffres-component></tricardoffres-component>
</section>
  <inclusoffres-component></inclusoffres-component>
<section class="bg-blue-100 p-2">
  <section class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-4 mt-12 mb-12">
  <section class="mt-6 grid grid-row-3 lg:grid-cols-2 gap-x-6 gap-y-8">
    <cardhome-component :bigcard="true" :item="{{json_encode($registres[0])}}" class="row-span-3">
    </cardhome-component>
    @for ($i = 1; $i < 4; $i++)
    <cardhome-component :bigcard="false" :item="{{json_encode($registres[$i])}}">
    </cardhome-component>
    @endfor
</section>
<button class="bg-buttonconfort mt-10 hover:bg-buttonessentiel text-white font-bold py-3 px-10 rounded-full text-sm focus:outline-none">Toutes nos réalisations</button>
</section>
</section>
<section class="w-4/5 m-auto" id="contact">
  <contact-component></contact-component>
</section>
</section>
@stop