<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Models\PhotoPresentation;
use App\Models\Registre;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $cardreal = [
            [
                "type" => "Essentielle",
                "description" =>
                "Projet écologique : Sauver les Pie de Châtelet",
                "dateD" => "05/09/2021",
                "dateF" => "05/12/2022",
                "statut" => "ouvert"
            ],
            [
                "type" => "Essentielle",
                "description" =>
                "Projet écologique : Sauver les Pie de Châtelet",
                "dateD" => "05/09/2021",
                "dateF" => "05/10/2021",
                "statut" => "clos"
            ],
            [
                "type" => "Performance",
                "description" => "Refonte de la bouche de métro du M1",
                "dateD" => "28/10/2022",
                "dateF" => "31/07/2023",
                "statut" => "ouvert"
            ],
            [
                "type" => "Premium",
                "description" => "Projet ailée de la Gare Saint-Lazare",
                "dateD" => "18/12/2020",
                "dateF" => "31/07/2023",
                "statut" => "ouvert"
            ]
        ];

        // $user = Visiteur::where("vis_id", 8)->select(["vis_ip"])->get()

        $reg = Registre::where("reg_maintenance", 0)->orderBy("reg_id", 'DESC')->skip(0)->take(4)->get();
        // $regouverture = Registre::select(["reg_ouverture"])->format('Y-m-d')->get();


        foreach ($reg as $key => $value) {

            if (file_exists(public_path("/storage/" . $value->reg_id . "/real" . $value->reg_id . ".jpg")))
                $value->imgboolean = true;
            else
                $value->imgboolean = false;
        }

        foreach ($reg as $key => $value) {
            $today = date("Y-m-d H:i:s");
            if (!$value->reg_fiche)
                $value->regstatut = "En attente validation fiche info";
            elseif ($today < $value->reg_ouverture)
                $value->regstatut = "En attente d'ouverture";
            elseif ($today >= $value->reg_ouverture && $today <= $value->reg_fermeture)
                $value->regstatut = "Ouvert";
            elseif ($today > $value->reg_fermeture) {
                if ($value->reg_date_archivage != '0000-00-00 00:00:00')
                    $date_archivage = $value->reg_date_archivage;
                else {
                    $date_archivage = date_create(substr($value->reg_ouverture, 0, 10));
                    date_modify($date_archivage, '+22 month');
                    $date_archivage = date_format($date_archivage, 'Y-m-d H:i:s');
                }

                if ($today >= $date_archivage)
                    $value->regstatut = "Archivé";
                else
                    $value->regstatut = "Clos";
            } else
                $value->regstatut = "Archivé";
        }
        return view("home", [
            "cardrea" => $cardreal,
            "registres" => $reg
            // "rouver" => $regouverture
        ]);
    }
}

// 