<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntervenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intervenant', function (Blueprint $table) {
            $table->increments('int_id');
            $table->string('int_civilite', 8);
            $table->string('int_prenom', 60);
            $table->string('int_nom', 60);
            $table->string('int_email', 120);
            $table->string('int_pass_crypt', 128);
            $table->string('int_pass_forget_key', 60);
            $table->date('int_pass_forget_key_expiration');
            $table->string('int_chg_email', 120);
            $table->string('int_chg_email_key', 60);
            $table->integer('int_connection_attempt');
            $table->dateTime('int_connection_last');
            $table->tinyInteger('int_administrateur');
            $table->integer('userg_ce_count_download');
            $table->integer('userg_aomo_count_download');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intervenant');
    }
}
