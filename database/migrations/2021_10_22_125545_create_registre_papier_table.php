<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrePapierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registre_papier', function (Blueprint $table) {
            $table->increments('regp_id');
            $table->integer('lie_id');
            $table->foreign('lie_id')->references('lie_id')->on('lieu_consultation')->onDelete('cascade');
            $table->string('regp_intitule', 256);
            $table->string('reg_nom_serveur', 60);
            $table->dateTime('regp_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registre_papier');
    }
}
