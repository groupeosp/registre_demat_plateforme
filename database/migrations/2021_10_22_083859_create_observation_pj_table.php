<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationPjTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observation_pj', function (Blueprint $table) {
            $table->increments('opj_id');
            $table->integer('obs_id');
            $table->foreign('obs_id')->references('obs_id')->on('observation')->onDelete('cascade');
            $table->integer('opj_numero');
            $table->string('opj_nom', 256);
            $table->string('opj_nom_serveur', 60);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observation_pj');
    }
}
