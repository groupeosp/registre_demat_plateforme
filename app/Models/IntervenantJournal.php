<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntervenantJournal extends Model
{
    protected $table = 'intervenant_journal';
}
