@extends('layouts.template')
@section('content')
<section class="bloc">
  <section class="bloc-premier md:mt-8 md:flex md:flex-row md:justify-items-center md:justify-center">
    <section class="w-full mt-16 flex flex-col gap-16">
        <article class="flex flex-col gap-5">
            <article class="font-bold text-blue-900 text-4xl"><h1>Plan du site <span class="text-yellow-600"> registredemat.fr</span></h1></article>
        </article>
        <article class="flex flex-row gap-16">
            <div class="container">
                <!-- Plan du site div starts -->
                <div class="animated bg-white fadeInDown flex-wrap mb-16 top-0 rounded-2xl shadow-lg p-4 w-full wow">
                    <div class="bg-transparent border-2 rounded-lg">
                        <!-- Accueil div starts -->
                        <div class="pt-4">
                            <div class="container text-justify">
                                <h2 class="mt-4 font-bold text-lg text-blue-900 wow fadeInUp animated">Accueil</h2>
                                <ul>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/" target="_blank">Index du site</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/cgu" target="_blank">Conditions générale d'utilisations</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/cgv" target="_blank">Conditions générale de vente</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/#footer" target="_blank">Mentions légales</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/#contact" target="_blank">Prendre contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Accueil div ends -->
                        <!-- Nos services div starts -->
                        <div class="pt-4">
                            <div class="container text-justify">
                                <h2 class="mt-4 font-bold text-lg text-blue-900 wow fadeInUp animated">Nos services</h2>
                                <ul>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/devis" target="_blank">Devis</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/offres" target="_blank">Nos offres</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/realisations" target="_blank">Nos réalisations</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Nos services div ends -->
                        <!-- Informations registre dématerialisé div starts -->
                        <div class="pt-4">
                            <div class="container text-justify">
                                <h2 class="mt-4 font-bold text-lg text-blue-900 wow fadeInUp animated">Informations registre dématerialisé</h2>
                                <ul>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/enquete_publique" target="_blank">Déroulement d'une enquête publique</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/concertation" target="_blank">Cadre légal concernant l'enquête publique</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/ppve" target="_blank">Tout savoir sur les enquêtes publiques</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/glossaire" target="_blank">Glossaire</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="/politique" target="_blank">Politique de confidentialité et de traitement des données</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Informations registre dématerialisé div ends -->
                        <!-- Réseaux sociaux div starts -->
                        <div class="pt-4">
                            <div class="container text-justify">
                                <h2 class="mt-4 font-bold text-lg text-blue-900 wow fadeInUp animated">Réseaux sociaux</h2>
                                <ul>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="https://twitter.com/share?url=https://www.registredemat.fr/" target="_blank">Partage Twitter</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="https://www.facebook.com/Registre-Demat-1511597192225488/" target="_blank">Partage Facebook</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="https://plus.google.com/share?url=https://www.registredemat.fr/" target="_blank">Partage Google+</a></li>
                                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><a href="https://linkedin.com/shareArticle?mini=ture&url=https://www.registredemat.fr/" target="_blank">Partage Linkedin</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Réseaux sociaux div ends -->
                    </div>
                </div>
                <!-- Plan du site div ends -->
            </div>
        </article>
    </section>
  </section>
</section>
@stop