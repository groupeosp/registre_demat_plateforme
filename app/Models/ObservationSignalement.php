<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ObservationSignalement extends Model
{
    protected $table = 'observation_signalement';
}
