<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Registre;

class RealisationsController extends Controller
{
    public function indexrea()
    {
        // $requestreal = Registre::whereNotIn('reg_id', [2, 4, 6])->where('reg_maintenance', 0)->get();
        // $reqdepart = Registre::join('lieu_consultation', 'registre.reg_id', '=', 'lieu_consultation.reg_id')->select('lieu_consultation.lie_departement')->get();

        // foreach ($requestreal as $key => $value) {

        //     if (file_exists(public_path("/storage/" . $value->reg_id . "/real" . $value->reg_id . ".jpg")))
        //         $value->imgboolean = true;
        //     else
        //         $value->imgboolean = false;
        // }

        // foreach ($requestreal as $key => $value) {
        //     $today = date("Y-m-d H:i:s");
        //     if (!$value->reg_fiche)
        //         $value->regstatut = "En attente validation fiche info";
        //     elseif ($today < $value->reg_ouverture)
        //         $value->regstatut = "En attente d'ouverture";
        //     elseif ($today >= $value->reg_ouverture && $today <= $value->reg_fermeture)
        //         $value->regstatut = "Ouvert";
        //     elseif ($today > $value->reg_fermeture) {
        //         if ($value->reg_date_archivage != '0000-00-00 00:00:00')
        //             $date_archivage = $value->reg_date_archivage;
        //         else {
        //             $date_archivage = date_create(substr($value->reg_ouverture, 0, 10));
        //             date_modify($date_archivage, '+22 month');
        //             $date_archivage = date_format($date_archivage, 'Y-m-d H:i:s');
        //         }

        //         if ($today >= $date_archivage)
        //             $value->regstatut = "Archivé";
        //         else
        //             $value->regstatut = "Clos";
        //     } else
        //         $value->regstatut = "Archivé";
        // }

        return view("showcase.pages.realisations" /*[
            "regreal" => $requestreal,
            "depreq" => $reqdepart

            // "rouver" => $regouverture
        ]*/);
    }

    function deprea(Request $request)
    {
        $requestreal = Registre::whereNotIn('reg_id', [2, 4, 6])->where('reg_maintenance', 0)->get();
        $reqdepart = Registre::join('lieu_consultation', 'registre.reg_id', '=', 'lieu_consultation.reg_id')
            ->when($request->departement != 0, function ($q) use ($request) {

                return $q->where('lieu_consultation.lie_departement', 'like', '%(' . $request->departement . ')%');
            })
            ->select('registre.*', 'lieu_consultation.lie_departement')
            ->get();

        foreach ($reqdepart as $key => $value) {

            if (file_exists(public_path("/storage/" . $value->reg_id . "/real" . $value->reg_id . ".jpg")))
                $value->imgboolean = true;
            else
                $value->imgboolean = false;
        }

        foreach ($reqdepart as $key => $value) {
            $today = date("Y-m-d H:i:s");
            if (!$value->reg_fiche)
                $value->regstatut = "En attente validation fiche info";
            elseif ($today < $value->reg_ouverture)
                $value->regstatut = "En attente d'ouverture";
            elseif ($today >= $value->reg_ouverture && $today <= $value->reg_fermeture)
                $value->regstatut = "Ouvert";
            elseif ($today > $value->reg_fermeture) {
                if ($value->reg_date_archivage != '0000-00-00 00:00:00')
                    $date_archivage = $value->reg_date_archivage;
                else {
                    $date_archivage = date_create(substr($value->reg_ouverture, 0, 10));
                    date_modify($date_archivage, '+22 month');
                    $date_archivage = date_format($date_archivage, 'Y-m-d H:i:s');
                }

                if ($today >= $date_archivage)
                    $value->regstatut = "Archivé";
                else
                    $value->regstatut = "Clos";
            } else
                $value->regstatut = "Archivé";
        }
        return $reqdepart;
    }
}
