<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request; 

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    //attributes 
    public $email; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->email = $request; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Contact')
                    ->from('testcontact95200@gmail.com')
                    ->to('nbache@osp.fr')
                    ->view('email.showcase.contactmail'); 
    }
}
