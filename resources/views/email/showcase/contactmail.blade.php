@extends('email.template')

@section('mail_fields')

    <h2 align="center">Contact</h2>
    <p>Réception d'une prise de contact avec les éléments suivants :</p>
    <ul align="left">
        <li><strong>Nom</strong> : {{ $email->name }}</li>
        <li><strong>Email</strong> : {{ $email->email }}</li>
        <li><strong>Téléphone</strong> : {{ $email->phone }}</li>
        <li><strong>Objet</strong> : {{ $email->subject }}</li>
        <li><strong>Message</strong> : {{ $email->content }}</li>
    </ul>
@endsection
