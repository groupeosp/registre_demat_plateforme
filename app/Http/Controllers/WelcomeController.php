<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        $bonjour = 'coucou';
        return view('test', ["cactus" => $bonjour]);
    }
}
