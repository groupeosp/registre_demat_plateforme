<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('doc_id');
            $table->integer('reg_id')->unsigned();
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->integer('doc_parent_id');
            $table->string('doc_num', 10);
            $table->string('doc_titre', 255);
            $table->string('doc_nom', 120);
            $table->string('doc_nom_serveur', 60);
            $table->string('doc_url_liseuse', 120);
            $table->integer('doc_count_download');
            $table->integer('doc_count_view');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
