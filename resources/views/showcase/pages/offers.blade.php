@extends('layouts.template')

@section('content')
{{-- Issu de la template de base et modifié pour faire la section Inclus dans nos offres --}}


<section class="services bg-gradient-to-t from-blue-100 via-white py-24 px-20">
  <div class="container">
    <div class="text-center">
    <article class="bloc-devis__titre text-xl md:text-left lg:text-left sm:text-4xl md:text-4xl lg:text-4xl font-bold text-blue-900"><h1>Inclus dans nos <span class="text-yellow-500">offres</span></h1></article>
    </div>
    <div class="flex flex-wrap">
      <!-- Services item -->
      <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
        <div class="m-4 wow fadeInRight animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
          <div class="icon text-5xl">
            <i class="lni lni-laptop-phone iconcol"></i>
          </div>
          <div>
            <h3 class="service-title">Web Responsive</h3>
            <p class="text-gray-600">Site web adaptatif pour PC, Tablette, Smartphone...</p>
          </div>
        </div>
      </div>
      <!-- Services item -->
      <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
        <div class="m-4 wow fadeInRight animated" data-wow-delay="0.6s" style="visibility: visible;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
          <div class="icon text-5xl">
            <i class="lni iconcol lni-users"></i>
          </div>
          <div>
            <h3 class="service-title">Sans limite d'utilisateurs</h3>
            <p class="text-gray-600">Possibilité d'utilisation pour autant de commissaires enquêteurs que souhaité</p>
          </div>
        </div>
      </div>
      <!-- Services item -->
      <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
        <div class="m-4 wow fadeInRight animated" data-wow-delay="0.9s" style="visibility: visible;-webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
          <div class="icon text-5xl">
            <i class="lni iconcol lni-stats-up"></i>
          </div>
          <div>
            <h3 class="service-title">OUTILS D'ANALYSE DE LA COMMISSION D'ENQUÊTE</h3>
            <p class="text-gray-600">Export, Outils d'analyse et Statistiques</p>
          </div>
        </div>
      </div>
      <!-- Services item -->
      <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
        <div class="m-4 wow fadeInRight animated" data-wow-delay="1.2s" style="visibility: visible;-webkit-animation-delay: 1.2s; -moz-animation-delay: 1.2s; animation-delay: 1.2s;">
          <div class="icon text-5xl">
            <i class="lni iconcol lni-dashboard"></i>
          </div>
          <div>
            <h3 class="service-title">MISE EN ŒUVRE RAPIDE</h3>
            <p class="text-gray-600">La mise en ligne rapide dès validation définitive du maître d’ouvrage ou de l’organisme référent</p>
          </div>
        </div>
      </div>
      <!-- Services item -->
      <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
        <div class="m-4 wow fadeInRight animated" data-wow-delay="1.5s" style="visibility: visible;-webkit-animation-delay: 1.5s; -moz-animation-delay: 1.5s; animation-delay: 1.5s;">
          <div class="icon text-5xl">
            <i class="lni iconcol lni-thumbs-up"></i>
          </div>
          <div>
            <h3 class="service-title">UTILISATION SIMPLE</h3>
            <p class="text-gray-600">Prise en main et appropriation rapide de l’outil par le commissaire enquêteur, 
                maître d’ouvrage ou organisme référent.</p>
          </div>
        </div>
      </div>
      <!-- Services item -->
      <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3">
        <div class="m-4 wow fadeInRight animated" data-wow-delay="1.8s" style="visibility: visible;-webkit-animation-delay: 1.8s; -moz-animation-delay: 1.8s; animation-delay: 1.8s;">
          <div class="icon text-5xl">
            <i class="lni iconcol lni-headphone-alt"></i>
          </div>
          <div>
            <h3 class="service-title">Hotline</h3>
            <p class="text-gray-600">Nous restons à disposition
              (Prix d’un appel local)</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
{{-- Code pour le tableau des offres --}}


<div class="bg-gradient-to-b from-blue-100 via-white">
 <section class="offers-table m-auto w-full pt-10 pb-24 ">
    <table class="4/5" align="center">
      <thead>
        <tr>
          <td colspan="4" align="center" class="pb-8"><img src="/images/nos-offres2.svg" height="30%" width="30%"></td>
        </tr>
        <tr>
          <th class="w-3/9"></th>
          <th class="w-2/9 capitalize md:uppercase text-white bg-colA1 py-4 border border-white">offre <b>essentielle</b></th>
          <th class="w-2/9 capitalize md:uppercase bg-colB1 text-white py-4 border border-white">offre <b>performance</b></th>
          <th class="w-2/9 capitalize md:uppercase bg-C1 text-white py-4 border border-white">offre <b>premium</b></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="bg-gray-200 font-semibold px-5 py-2">Registre dématérialisé sécurisé</td>
          <td class="border border-white py-2 bg-colA1"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-colB2"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C2"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-50 font-semibold px-5 py-2">Outils d'analyse et de statistiques (CE/AO/MO)</td>
          <td class="border border-white py-2 bg-colA2"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-colB3"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C3"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-200 font-semibold px-5 py-2">Réception quotidienne par courriel des observations déposées la veille</td>
          <td class="border border-white py-2 bg-colA3"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-colB4"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C4"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-50 font-semibold px-5 py-2">Personnalisation du logo et couleurs du registre</td>
          <td class="border border-white py-2 bg-colA4"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-colB5"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C5"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-200 font-semibold px-5 py-2">Système de modération paramétrable</td>
          <td class="border border-white py-2 bg-colA5"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-colB6"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C6"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-50 font-semibold px-5 py-2">Assistance téléphonique gratuite</td>
          <td class="border border-white py-2 bg-colA6"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-colB7"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C7"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-200 font-semibold px-5 py-2">Création de site et mise en ligne des documents de l'enquête</td>
          <td class="border border-white py-2"></td>
          <td class="border border-white py-2 bg-colB8"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C8"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-50 font-semibold px-5 py-2">Document accessibles en visionnage et téléchargement</td>
          <td class="border border-white py-2"></td>
          <td class="border border-white py-2 bg-colB9"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C9"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-200 font-semibold px-5 py-2">Mise en ligne du rapport du commissaire enquêteur</td>
          <td class="border border-white py-2"></td>
          <td class="border border-white py-2 bg-colB10"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C10"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-50 font-semibold px-5 py-2">Mise en ligne des scans des registres papier</td>
          <td class="border border-white py-2"></td>
          <td class="border border-white py-2 bg-colB11"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
          <td class="border border-white py-2 bg-C11"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-200 font-semibold px-5 py-2">Création site sur-mesure</td>
          <td class="border border-white py-2"></td>
          <td class="border border-white py-2"></td>
          <td class="border border-white py-2 bg-C12"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-50 font-semibold px-5 py-2">Nom de domaine dédié</td>
          <td class="border border-white py-2"></td>
          <td class="border border-white py-2"></td>
          <td class="border border-white py-2 bg-C13"><section class="flex justify-center"><img src="/images/logoVoffers.svg" height="8%" width="8%"></section></td>
        </tr>
        <tr>
          <td class="bg-gray-200 font-semibold px-5 py-2">Adresse courriel dédiée</td>
          <td class="bg-gray-50 font-semibold text-center border border-white py-2">Option</td>
          <td class="bg-gray-50 font-semibold text-center border border-white py-2">Option</td>
          <td class="bg-gray-50 font-semibold text-center border border-white py-2">Option</td>
        </tr>
        <tr>
          <td class="bg-gray-50 font-semibold px-5 py-2">Imports des courriels sur le registre dématérialisé</td>
          <td class="bg-gray-50 font-semibold text-center border border-white py-2">Option</td>
          <td class="bg-gray-50 font-semibold text-center border border-white py-2">Option</td>
          <td class="bg-gray-50 font-semibold text-center border border-white py-2">Option</td>
        </tr>
      </tbody>
    </table>
  </section>
  <section class="offers-table2">
<example-component></example-component>
  </section>
  <div class="py-16 text-center">
    <a href="{{ route('devis') }}" class="btn_blue">Demander un devis gratuitement</a>
  </div>
</div>
@stop