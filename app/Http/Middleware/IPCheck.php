<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Auth;

class IPCheck
{
    /**
     * Handle an incoming request.
     *okeefe.nayeli@example.net
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip = request()->ip();

        // Adresses IP ayant accès à l'admin : 89.90.211.129=>OSP DIRECT et 89.90.211.130=>OSP PROXY
        $ip_granted = \Config::get('constants.ip_granted');

        /**
         * Redirection vers osp.fr si l'adresse ip d'ou provient la requête ne figure ou si l'utilisateur n'est pas connecté
         * pas dans la liste d'ip autorisées
         */
        if (!in_array($ip, $ip_granted)) {
            $domain = env('APP_ENV') == 'staging' ? env('APP_DOMAIN') : 'registredematerialise.com';
            $request->headers->set('Host', 'www.' . $domain);

            return redirect()->secure('/', 301);
        }
        return $next($request);
    }
}
