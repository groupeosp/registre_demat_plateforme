<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntAccessFonction extends Model
{
    protected $table = 'int_access_fonction';
}
