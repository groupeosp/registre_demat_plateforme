<?php

use App\Http\Controllers\homeController;
use App\Http\Controllers\RealisationsController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\WelcomeController;

use App\Mail\ContactMail;
use App\Mail\DevisMail;
use App\Mail\TestdevisMail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'ipRestricted'], function () {
    //home page route
    Route::get('/', [homeController::class, 'index'])->name('home');
    //route controller de realisation
    Route::get('/realisations', [RealisationsController::class, 'indexrea'])->name('realisations');
    // route axios 
    Route::post('/realisations', [RealisationsController::class, 'deprea']);


    //route vers la page test
    Route::get('/test', function () {
        return view('showcase.pages.test');
    })->name('test');

    //login page route 
    Route::get('/connexion', function () {
        return view('showcase.pages.auth.login');
    })->name('login');

    //devis page route 
    Route::get('/cgv', function () {
        return view('showcase.pages.cgv');
    })->name('cgv');

    //devis page route 
    Route::get('/cgu', function () {
        return view('showcase.pages.cgu');
    })->name('cgu');

    //devis page route 
    Route::get('/devis', function () {
        return view('showcase.pages.devis');
    })->name('devis');

    Route::post('/devis', function (Request $request) {
        Mail::send(new DevisMail($request));
        return redirect('/');
    });


    //offers page route
    Route::get('/offres', function () {
        return view('showcase.pages.offers');
    })->name('offers');

    //Contact page routes 
    Route::get('/contact', function () {
        return view('showcase.pages.contact');
    })->name('contact');

    Route::post('/contact', function (Request $request) {
        Mail::send(new ContactMail($request));
        return redirect('/');
    });

    //enquetes publiques page route 
    Route::get('/enquete_publique', function () {
        return view('showcase.pages.solutions.public_survey');
    })->name('ps');

    //concertation page route 
    Route::get('/concertation', function () {
        return view('showcase.pages.solutions.concertation');
    })->name('concertation');

    //participation du public par voie electronique page route 
    Route::get('/ppve', function () {
        return view('showcase.pages.solutions.ppve');
    })->name('ppve');

    //glossaire page route
    Route::get('/glossaire', function () {
        return view('showcase.pages.glossaire');
    })->name('glossaire');

    //plan du site page route
    Route::get('/site_map', function () {
        return view('showcase.pages.site_map');
    })->name('site_map');

    //politique page route 
    Route::get('/politique-de-confidentialite', function () {
        return view('showcase.pages.politique');
    })->name('politique');

    //politique page plan du site 
    Route::get('/plan_du_site', function () {
        return view('showcase.pages.plan');
    })->name('plan');
});
