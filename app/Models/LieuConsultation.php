<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LieuConsultation extends Model
{
    protected $table = 'lieu_consultation';
}
