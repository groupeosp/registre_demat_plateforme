<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermanenceVisio extends Model
{
    protected $table = 'permanences_visio';
}
