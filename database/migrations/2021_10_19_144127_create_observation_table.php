<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observation', function (Blueprint $table) {
            $table->increments('obs_id');
            $table->integer('reg_id')->unsigned();
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->integer('obs_numero');
            $table->integer('obs_type');
            $table->integer('obs_lieu');
            $table->tinyInteger('obs_qualite');
            $table->string('obs_nom', 80);
            $table->string('obs_prenom', 80);
            $table->string('obs_email', 80);
            $table->boolean('obs_diffusion_coord');
            $table->string('obs_adresse', 120);
            $table->string('obs_cp', 10);
            $table->string('obs_ville', 80);
            $table->text('obs_observation');
            $table->text('obs_reponse');
            $table->boolean('obs_reponse_informe');
            $table->dateTime('obs_date');
            $table->string('obs_pj_nom', 256);
            $table->string('obs_pj_nom_serveur', 60);
            $table->string('obs_cle_validation', 60);
            $table->boolean('obs_moderation');
            $table->string('obs_ip', 40);
            $table->integer('obs_priorite');
            $table->integer('obs_avis');
            $table->integer('obs_moder_int');
            $table->dateTime('obs_moder_date');
            $table->integer('obs_modif_int');
            $table->dateTime('obs_modif_date');
            $table->integer('obs_integr_int');
            $table->dateTime('obs_integr_date');
            $table->integer('obs_info_pub_rapport');
            $table->integer('obs_doublon');
            $table->string('obs_mail_id', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observation');
    }
}
