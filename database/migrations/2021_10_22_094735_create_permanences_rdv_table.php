<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermanencesRdvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permanences_rdv', function (Blueprint $table) {
            $table->increments('rdv_id');
            $table->integer('per_id');
            $table->foreign('per_id')->references('per_id')->on('permanences')->onDelete('cascade');
            $table->integer('obs_numero');
            $table->dateTime('rdv_date');
            $table->string('rdv_prenom', 60);
            $table->string('rdv_nom', 60);
            $table->string('rdv_email', 120);
            $table->string('rdv_tel', 20);
            $table->integer('rdv_cree_par');
            $table->dateTime('rdv_cree_le');
            $table->string('rdv_modif_par', 40);
            $table->dateTime('rdv_modif_le');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permanences_rdv');
    }
}
