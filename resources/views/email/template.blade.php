<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		<head>
			<!--[if gte mso 9]>
				<xml>
					<o:OfficeDocumentSettings>
					<o:AllowPNG/>
					<o:PixelsPerInch>96</o:PixelsPerInch>
					</o:OfficeDocumentSettings>
				</xml>
			<![endif]-->
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<meta name="x-apple-disable-message-reformatting">
			<!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
			<title>Contact</title>
			<style type="text/css">
				table, td { color: #000000; } 
				a { color: #0000ee; text-decoration: underline; }
				@media only screen and (min-width: 620px) {
				  .u-row {	width: 600px !important;  }
				  .u-row .u-col { vertical-align: top;  }
				  .u-row .u-col-100 { width: 600px !important;  }
				}
		
				@media (max-width: 620px) {
				  .u-row-container { max-width: 100% !important; padding-left: 0px !important; padding-right: 0px !important; }
				  .u-row .u-col { min-width: 320px !important; max-width: 100% !important; display: block !important; }
				  .u-row { width: calc(100% - 40px) !important; }
				  .u-col { width: 100% !important; }
				  .u-col > div { margin: 0 auto; }
				}
				body { margin: 0; padding: 0; font-family:helvetica, arial, sans-serif; }
				table, tr, td { vertical-align: top; border-collapse: collapse; }
				p { margin: 0; }
				.ie-container table, .mso-container table { table-layout: fixed; }
				* { line-height: inherit; }
				a[x-apple-data-detectors="true"] { color: inherit !important; text-decoration: none !important;	}
				@media (max-width: 480px) { 
					.hide-mobile { display: none !important; max-height: 0px; overflow: hidden; }
				}
				.observation { margin-top: 0; overflow: hidden; text-align: justify; }
				.numero { text-align: left; font-style: italic;	color:#393939; font-weight: 700; text-transform: uppercase;	text-align: left; }
				.rouge { color:#fe415d; font-weight: 400; text-transform: none;	text-align: left; font-style: italic; }
				.pj { background-color: #fe415d; font-size: 8pt; color: #fff; text-align: center; font-weight: 700;	text-transform: uppercase; border-radius: 18pt;	text-decoration: none; padding: 9pt 4pt; }
			</style>
		</head>
		<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
			<!--[if IE]><div class="ie-container"><![endif]-->
			<!--[if mso]><div class="mso-container"><![endif]-->
			<table style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr style="vertical-align: top">
						<td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
							<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
							<div class="u-row-container" style="padding: 0px;background-color: transparent">
								<div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ecf0f1;">
									<div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
										<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px;"><tr style="background-color: #ecf0f1;"><![endif]-->
										<!--[if (mso)|(IE)]><td align="center" width="600" style="width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
										<div class="u-col u-col-100" style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
											<div style="width: 100% !important;">
												<!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
												<table style="" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
													<tbody>
														<tr>
															<td style="overflow-wrap:break-word;word-break:break-word;padding:10px;" align="left">
																<table width="100%" cellpadding="0" cellspacing="0" border="0">
																	<tr>
																		<td align="center" style="padding-right: 0px;padding-left: 0px;">
																			<img src="https://www.registredematerialise.com/images/logoB.svg" alt="Logo Registre Demat." title="Image" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 100%;max-width: 200px;" width="200"/>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
												<!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
											</div>
										</div>
										<!--[if (mso)|(IE)]></td><![endif]-->
										<!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
									</div>
								</div>
							</div>
							<div class="u-row-container" style="padding: 0px;background-color: transparent">
								<div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #6cbad1;">
									<div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
										<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px;"><tr style="background-color: #6cbad1;"><![endif]-->
										<!--[if (mso)|(IE)]><td align="center" width="600" style="width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
										<div class="u-col u-col-100" style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
											<div style="width: 100% !important;">
												<!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
												<table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
													<tbody>
														<tr>
															<td style="overflow-wrap:break-word;word-break:break-word;padding:10px;" align="left">
																<div class="menu" style="text-align:center">
																	<!--[if (mso)|(IE)]><table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center"><tr><![endif]-->
																	<!--[if (mso)|(IE)]><td style="padding:4px 15px"><![endif]-->
																	<span style="padding:2px 10px;display:inline;color:#ffffff;font-family:\'Raleway\',sans-serif;font-size:12px">
																		La solution rapide et fiable pour créer simplement un registre dématérialisé en ligne.
																	</span>
																	<!--[if (mso)|(IE)]></td><![endif]-->
																	<!--[if (mso)|(IE)]></tr></table><![endif]-->
																</div>
															</td>
														</tr>
													</tbody>
												</table>
												<!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
											</div>
										</div>
										<!--[if (mso)|(IE)]></td><![endif]-->
										<!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
									</div>
								</div>
							</div>
							<div class="u-row-container" style="padding: 0px;background-color: transparent">
								<div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ecf0f1;">
									<div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
										<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px;"><tr style="background-color: #ecf0f1;"><![endif]-->
										<!--[if (mso)|(IE)]><td align="center" width="600" style="width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
										<div class="u-col u-col-100" style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
											<div style="width: 100% !important;">
												<!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
												<table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
													<tbody>
														<tr>
															<td style="overflow-wrap:break-word;word-break:break-word;padding:30px 10px 15px 25px;">
																<div style="line-height: 140%; word-wrap: break-word;">
																	<p style="font-size: 12px; line-height: 140%;">
                                                                        <div>
                                                                            @yield('mail_fields')
                                                                        </div>
																		{{-- <span style="line-height: 24px; font-family: \'book antiqua\', palatino; color: #000000; font-size: 24px;">'.$type.'</span> --}}
																	</p>
																</div>	
															</td>
														</tr>
													</tbody>
												</table>
												<table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
													<tbody>
														<tr>
															<td style="overflow-wrap:break-word;word-break:break-word;padding:30px 40px 10px;" align="left">
																<div style="font-size: 14px; text-align: left; word-wrap: break-word;">
																	{{-- '.nl2br($contents).'
																	<p>&nbsp;</p> --}}
																</div>
															</td>
														</tr>
													</tbody>
												</table>
												<!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
											</div>
										</div>
										<!--[if (mso)|(IE)]></td><![endif]-->
										<!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
									</div>
								</div>
							</div>
							<div class="u-row-container" style="padding: 0px;background-color: transparent">
								<div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #383843;">
									<div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
										<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px;"><tr style="background-color: #000000;"><![endif]-->
										<!--[if (mso)|(IE)]><td align="center" width="600" style="width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
										<div class="u-col u-col-100" style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
											<div style="width: 100% !important;">
												<!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
												<table style="border-bottom: 1px solid #6e7074;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
													<tbody>
														<tr>
															<td style="overflow-wrap:break-word;word-break:break-word;padding:10px 40px;" align="left">
																<div style="line-height: 140%; text-align: left; word-wrap: break-word;">
																	<p style="font-size: 14px; line-height: 140%; text-align: center;">
																		<div style="color: #ffffff; font-size: 14px; line-height: 19.6px; text-align: center;">Ce courriel a été envoyé automatiquement par un robot, <br/>merci de ne pas y répondre.</div>
																		<span>&#160;</span>
																	</p>
																</div>
															</td>
														</tr>
													</tbody>
												</table>
												<table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
													<tbody>
														<tr>
															<td style="overflow-wrap:break-word;word-break:break-word;padding:7px 40px 5px;" align="left">
																<div style="line-height: 140%; text-align: left; word-wrap: break-word;">
																	<p style="font-size: 14px; line-height: 140%; text-align: center;"><span style="color: #ffffff; font-size: 12px; line-height: 16.8px;">Copyright &copy; Legalcom &nbsp;| &nbsp; Tous droits réservés.</span></p>
																</div>
															</td>
														</tr>
													</tbody>
												</table>
												<table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
													<tbody>
														<tr>
															<td style="overflow-wrap:break-word;word-break:break-word;padding:7px 40px 20px;" align="left">
																<div style="line-height: 140%; text-align: left; word-wrap: break-word;">
																	{{-- <p style="font-size: 14px; line-height: 140%; text-align: center;"><span style="color: #999999; font-size: 12px; line-height: 16.8px;">'.$footer1.'</span></p>
																	<p style="font-size: 14px; line-height: 140%; text-align: center;"><span style="color: #999999; font-size: 12px; line-height: 16.8px;">'.$footer2.'</span></p> --}}
																</div>
															</td>
														</tr>
													</tbody>
												</table>
												<!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
											</div>
										</div>
										<!--[if (mso)|(IE)]></td><![endif]-->
										<!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
									</div>
								</div>
							</div>
							<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
						</td>
					</tr>
				</tbody>
			</table>
			<!--[if mso]></div><![endif]-->
			<!--[if IE]></div><![endif]-->
		</body>
	</html>