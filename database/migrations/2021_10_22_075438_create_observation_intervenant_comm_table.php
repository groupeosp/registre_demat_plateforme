<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationIntervenantCommTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observation_intervenant_comm', function (Blueprint $table) {
            $table->integer('obs_id');
            $table->foreign('obs_id')->references('obs_id')->on('observation')->onDelete('cascade');
            $table->integer('int_id');
            $table->foreign('int_id')->references('int_id')->on('intervenant')->onDelete('cascade');
            $table->text('oic_commentaire');
            $table->dateTime('oic_date_modif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observation_intervenant_comm');
    }
}
