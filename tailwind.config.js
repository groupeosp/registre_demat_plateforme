// tailwind.config.js
const colors = require('tailwindcss/colors')
module.exports = {
  purge: [
    './storage/framework/views/*.php',
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    },
    extend: {
      colors: {
        'conttitre' :'#2F327D',
        'conttext' :'#696984',
        'contform' :'#F1F1F1',
        'textdesc' :'#696984',
        'buttonconfort' : '#5B72EE',
        'buttonpremium' : '#F48C06',
        'buttonessentiel' : '#29B9E7',
        'colA1':'#6ABAD2',
        'colA2':'#6ABAD2',
        'colA3':'#65B6CE',
        'colA4':'#57ABC3',
        'colA5':'#4CA2BA',
        'colA6':'#3C95AD',
        'colA7':'#338FA6',
        'colB1':'#2695B2',
        'colB2':'#2695B2',
        'colB3':'#238AAE',
        'colB4':'#207FA9',
        'colB5':'#1C6EA2',
        'colB6':'#19639D',
        'colB7':'#165899',
        'colB8':'#145095',
        'colB9':'#124893',
        'colB10':'#0F3C8E',
        'colB11':'#0D378B',
        'titreoff':'#0B90AE',
        'C1':'#949ED1',
        'C2':'#949ED1',
        'C3':'#8D93C8',
        'C4':'#8382BB',
        'C5':'#7971AF',
        'C6':'#6F61A2',
        'C7':'#69589C',
        'C8':'#655197',
        'C9':'#604890',
        'C10':'#5D448D',
        'C11':'#5A3E88',
        'C12':'#563884',
        'C13':'#583882',
        'lime500' : '#84CC16', 
        'sky600' : '#0284C7',
        'amber500' : '#F59E0B',
        'cyan800' : '#155E75'
      },
      backgroundImage: {
        'snake': "url('/images/home/snake.svg')",
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}