@extends('layouts.template')
@section('content')
<section class="bg-blue-100 pt-4 pb-4 ">
<div class="bloc-premier rounded-xl shadow-lg bg-white p-16 textcgv">
    <h2>PREAMBULE</h2>
    Les présentes conditions générales de vente (CGV) s'appliquent aux commandes de logiciels de registre dématérialisé passées entre la société LEGALCOM et tout client, qu’il soit l’autorité organisatrice de l’enquête publique, son mandataire ou commissaire-enquêteur.
    <br />
    <br />
    Il convient d’entendre par « registre dématérialisé » : un programme informatique accessible et utilisable à l'aide de l’outil Internet.
    <br />
    <br />
    LEGALCOM se réserve le droit de modifier le contenu des présentes conditions générales de vente.
    <br />
    <br />
    L’inscription aux services de www.registredemat.fr  implique l'acceptation pleine et sans réserve du client de l'intégralité des présentes conditions générales de vente.
    <br />
    <br />
    Le client déclare avoir la capacité juridique lui permettant d'effectuer une commande auprès de LEGALCOM.
    <br />
    <br />
    Sauf preuve contraire, les données enregistrées par www.registredemat.fr  constituent la preuve de l'ensemble des transactions passées entre LEGALCOM et ses clients.
    <br />
    <br />
    <h2>ARTICLE 1 - CARACTERISTIQUES DU CONTRAT</h2>
    Le contrat conclu entre LEGALCOM et le client est matérialisé par la signature du BON DE COMMANDE correspondant à la configuration et à l’exploitation de(s) logiciel(s) de registre dématérialisé choisi(s) par le client.
    <br />
    <br />
    Le client ayant souscrit un contrat a accès uniquement au(x) logiciel(s) objet dudit contrat.
    <br />
    <br />
    Le contrat est souscrit pour la durée d’exploitation du logiciel déterminée par le client lui-même. La seule consultation des données restant accessible pendant une durée de un an à compter de la création du registre dématérialisé.
    <br />
    <br />
    <h2>ARTICLE 2 - PRIX ET FACTURATION</h2>
    Les prix des services proposés par LEGALCOM sont indiqués dans la rubrique « Tarifs » du site www.registredemat.fr. Ils sont exprimés hors taxe en euros. La facturation faite au client sera donc majorée des taxes en vigueur à la date de facturation.
    <br />
    <br />
    Le client a la possibilité de régler ses factures par chèque ou par virement bancaire. Les factures émises par LEGALCOM sont payables 30 jours fin de mois. Toute facture non réglée à l'échéance porte intérêt, de plein droit sans mise en demeure préalable, à 1,5  fois le taux légal en vigueur
    <br />
    <br />
    En application de la loi du 22 mars 2012, une indemnisation forfaitaire de 40 € sera appliquée si  le paiement est effectué après la date d’échéance.
    <br />
    <br />
    <h2>ARTICLE 3 - OBLIGATIONS DU CLIENT</h2>
    Le client est seul responsable des données et fichiers diffusés et collectés via le logiciel de registre dématérialisé ainsi que de leur exploitation.
    <br />
    <br />
    Le client reconnaît avoir vérifié l'adéquation du service à ses besoins et notamment l'équipement et la connexion Internet nécessaires à l'utilisation des services proposés par www.registredemat.fr.
    Le client s'engage à respecter l'ensemble des réglementations en vigueur, et en particulier celles relatives à la déclaration des traitements des fichiers de données à caractère personnel auprès de la Commission Nationale de l'Informatique et des Libertés (C.N.I.L.).
    <br />
    <br />
    <h2>ARTICLE 4 - OBLIGATIONS DE LEGALCOM</h2>
    LEGALCOM s’engage à fournir un accès optimal au logiciel 24 heures sur 24, 7 jours sur 7. Cette obligation étant une obligation de moyens.
    <br />
    <br />
    LEGALCOM ne pourra être tenu responsable des dommages subis par le client suite à l'indisponibilité temporaire des services rendue indispensable par les opérations de maintenance et de mise à jour des logiciels.
    <br />
    <br />
    LEGALCOM se réserve le droit de refuser la présence sur ses serveurs, de fichiers importés par le client qui seraient jugés techniquement non conformes ou nuisibles à ces derniers.
    <br />
    <br />
    LEGALCOM  s'engage à prendre toute précaution raisonnable pour assurer la protection matérielle des données et programmes.
    <br />
    <br />
    <h2>ARTICLE 5 - PROPRIETE INTELLECTUELLE</h2>
    Les éléments accessibles sur le site www.registredemat.fr tels que les logiciels, bases de données, textes, images, photographies, et plus généralement l'ensemble des informations mises à la disposition du client dans le cadre du contrat, sont la propriété exclusive de LEGALCOM.
    Le client ne peut, en aucun cas, reproduire, modifier ou exploiter de quelque manière que ce soit, ces éléments sans l'autorisation écrite préalable de LEGALCOM.
    <br />
    <br />
    <h2>ARTICLE 6 – CONFIDENTIALITE</h2>
    Les informations personnelles relatives au client (données société, activité, coordonnées, etc…) sont exclusivement réservées à la gestion des relations commerciales entre LEGALCOM et le client.
    <br />
    <br />
    Les données appartenant au client et hébergées par www.registredemat.fr demeurent la seule propriété du client.
    <br />
    <br />
    LEGALCOM s'engage à garder ces données confidentielles, à n'en effectuer, en dehors des nécessités techniques, aucune copie et à n'en faire aucune utilisation autre que celles prévues pour l'exécution de la prestation.
    L'administrateur de www.registredemat.fr est seul habilité à travailler sur les bases de données brutes pour les besoins techniques de maintenance et de mise à jour.
    <br />
    <br />
    Le client reconnait quant à lui que les identifiants d’accès au logiciel sont strictement personnels et confidentiels. Il s'engage ainsi à ne pas les divulguer à des tiers.
    L’usage des identifiants se fait sous l'entière responsabilité du client.
    <br />
    <br />
    <h2>ARTICLE  7 - PROTECTION DES DONNEES A CARACTERE PERSONNEL</h2>
    Conformément à la loi informatique et libertés du 6 janvier 1978, le client dispose d'un droit d'accès, de rectification et d'opposition aux données personnelles le concernant.
    <br />
    <br />
    Pour exercer ce droit, il suffit au client d'en faire la demande en indiquant ses nom, prénom, adresse et si possible sa référence client à l’adresse postale : LEGALCOM - 14 rue Beffroy - CS 30018 - 92523 Neuilly-sur-Seine Cedex.
    <br />
    <br />
    <h2>ARTICLE 8 - RESILIATION DU CONTRAT</h2>
    Le contrat peut être résilié à tout moment par le client qui en fait la demande expresse à LEGALCOM.
    Dans ce cas, aucune somme ne sera restituée au client. Ce dernier sera donc tenu de régler toutes les échéances restant dues entre la demande de résiliation et la date de fin du contrat.
    <br />
    <br />
    LEGALCOM  se réserve le droit de résilier le contrat à tout moment en cas de non-respect par le client de ses obligations.
    Dans ce cas,  LEGALCOM s'engage à conserver et tenir à la disposition du client les données pendant une période de trente jours à compter de l'interruption du service. Passé ce délai, LEGALCOM pourra détruire les données sans que le client ne puisse prétendre à une quelconque réparation.
    <br />
    <br />
    <h2>ARTICLE 9 - DROIT APPLICABLE – LITIGES</h2>
    Les présentes conditions générales de vente sont régies par la loi française.
    Tous litiges relatifs à l'interprétation ou à l'exécution des présentes conditions générales de vente seront soumis à la compétence du Tribunal de Commerce de Nanterre.
    <br />
    <br />
</div>
</section>
@stop