<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ObservationIntervenantComm extends Model
{
    protected $table = 'observation_intervenant_comm';
}
