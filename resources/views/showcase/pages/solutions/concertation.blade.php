@extends('layouts.template')
@section('content')
<section class="bloc">
  <section class="bloc-premier md:mt-8 md:flex md:flex-row md:justify-items-center md:justify-center">
    <section class="w-full mt-16 flex flex-col gap-16">
      <article class="flex flex-col gap-5">
        <article class="font-bold text-blue-900 text-4xl"><h1>Concertations <span class="text-yellow-500">Dématerialisées</span></h1></article>
      </article>
      <article class="flex flex-row">
      <div class="container" id="concertations">
        <div class="animated bg-white fadeInDown flex-wrap mb-16 top-0 rounded-2xl shadow-lg space-y-3 p-4 w-full wow">
            <div class="container text-justify">
                <h2 class="mt-8 font-bold text-lg uppercase text-blue-900 wow fadeInUp animated">Collectivités, décideurs publics, maîtres d'ouvrage, </h2>
                <p data-wow-delay="0.6s" class="mb-1 text-gray-900 leading-6 text-base wow fadeInUp">vous avez de nombreux projets à forts enjeux urbains, environnementaux…</p>
            </div>
            <div class="container text-justify">
                <h2 class="mt-8 font-bold text-lg uppercase text-blue-900 wow fadeInUp animated">Les indispensables de la concertation dématérialisée</h2>
                <p data-wow-delay="0.6s" class="inline mb-1 text-gray-900 leading-6 text-base wow fadeInUp">Pour les mener à bien dans les meilleures conditions, vous savez que <p class="inline text-yellow-600">l’adhésion citoyenne</p> est un gage de réussite. La concertation est alors une étape primordiale dans la réussite de votre futur projet.</p>
            </div>
            <div class="container text-justify">
                <p data-wow-delay="0.6s" class="inline mb-1 text-gray-900 leading-6 text-base wow fadeInUp"><p class="inline text-yellow-600">Acteur majeur de l’expression citoyenne</p>, LEGALCOM a créé des outils modernes vous permettant de communiquer pour :</p>
                <ul>
                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><p class="inline text-yellow-600">Présenter</p> votre projet,</li>
                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><p class="inline text-yellow-600">Informer/questionner</p> les parties prenantes,</li>
                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><p class="inline text-yellow-600">Répondre</p> au public.</li>
                </ul>
            </div>
            <div class="container text-justify">
                <p data-wow-delay="0.6s" class="inline mb-1 text-gray-900 leading-6 text-base wow fadeInUp">LEGALCOM vous propose de faire de votre concertation une action de communication et d’échanges au travers d’un dialogue facilité.</p>
            </div>
            <div class="container text-justify">
                <p data-wow-delay="0.6s" class="inline mb-1 text-gray-900 leading-6 text-base wow fadeInUp">Nous concevons pour vous le site internet dédié à votre consultation, incluant un espace d’échanges permettant au public concerné de s’exprimer, donner son avis, faire des propositions…</p>
            </div>
            <div class="container text-justify">
                <h2 class="mt-8 font-bold text-lg uppercase text-blue-900 wow fadeInUp animated">Les champs personnalisés</h2>
                <p data-wow-delay="0.6s" class="inline mb-1 text-gray-900 leading-6 text-base wow fadeInUp">En complément du formulaire, des champs personnalisés peuvent être ajoutés pour une meilleure étude des observations par le garant.</p>
            </div>
            <div class="container text-justify">
                <h2 class="mt-8 font-bold text-lg uppercase text-blue-900 wow fadeInUp animated">Vos avantages sont multiples :</h2>
                <ul class="">
                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><p class="inline text-yellow-600">Impact positif</p> sur votre image (communiquant et moderne),</li>
                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><p class="inline text-yellow-600">Sonder le public</p> pour connaître le degré d’adhésion/résistance au projet,</li>
                    <li><span class="text-blue-900 rounded-full text-4xl font-bold px-3 py-2">.</span><p class="inline text-yellow-600">Adapter votre projet</p> aux oppositions soulevées, intégrer les « bonnes idées » … et ce pour prendre les meilleures décisions quant à la mise en place de votre futur projet.</li>
                </ul>
            </div>
            <div class="text-center justify-center m-8 space-y-8">
                <h2 class="mt-4 bg-blue-900 font-bold text-lg text-white wow fadeInUp animated">Besoin d'un conseil </h2>
                <p data-wow-delay="0.6s" class="mb-1 text-gray-600 leading-6 text-base wow fadeInUp">Votre projet nécessite une enquête publique et vous souhaitez profiter de cette occasion pour communiquer. Appelez-nous, prenons rendez-vous! Notre équipe saura vous conseiller pour offrir une solution adaptée à vos besoins. </p>
                <div class="text-center mb-1 wow fadeInUp" data-wow-delay="1.2s">
                    <a href="/contact" target="blank" rel="nofollow" class=" rounded-full font-bold px-3 py-2 bg-blue-900 text-white">Nous contacter</a>
                </div>
            </div>
        </div>
      </div>
      </article>
    </section>
  </section>
</section>
@stop