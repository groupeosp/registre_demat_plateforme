<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Illuminate\Http\Request; 

class DevisMail extends Mailable
{
    use Queueable, SerializesModels;

    //devis attributes 
    public $devis; 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->devis = $request; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Demande de devis')
                    ->from('testcontact95200@gmail.com')
                    ->to('nbache@osp.fr')
                    ->view('email.showcase.devismail'); 
    }
}
