@extends('email.template')

@section('mail_fields')

    <h2 align="center">Devis</h2>
    <p>Concernant l'utilisateur : </p>
    <ul align="left">
        <li><strong>Nom</strong> : {{ $devis->name }}</li>
        <li><strong>Prénom</strong> : {{ $devis->firstname }}</li>
        <li><strong>Fonction</strong> : {{ $devis->fonction }}</li>
        <li><strong>Email</strong> : {{ $devis->email }}</li>
        <li><strong>Tél</strong> : {{ $devis->phone }}</li>
    </ul>
    <p>Concernant le devis : </p>
    <ul align="left">
        <li><strong>Offre</strong> : {{ $devis->offer }}</li>
        <li><strong>Type d'enquête</strong> : {{ $devis->survtype }}</li>
        <li><strong>Autres infos</strong> : {{ $devis->otherinfos }}</li>
    </ul>
@endsection
