<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurligneurMot extends Model
{
    protected $table = 'surligneur_mot';
}
