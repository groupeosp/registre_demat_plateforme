<!doctype html>
<html lang="fr">
<head>
   @include('showcase.partials.head')
</head>
<body>
    <div id="app">
        <header class="row">
            @include('showcase.partials.nav')
        </header>
        <div id="main" class="row">
                @yield('content')
        </div>
        <footer>
            @include('showcase.partials.footer')
            @include('showcase.partials.js_scripts')
        </footer>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    
</body>
</html>