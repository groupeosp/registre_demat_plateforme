<!doctype html>
<html lang="fr">
<head>
   @include('showcase.partials.head')
</head>
<body class="body-template">
    <div id="app">
        <header class="row">
            @include('showcase.partials.nav')
        </header>
        <div id="main" class="row">
            @yield('content')
        </div>
        <footer>
            <footer-component></footer-component>
            @include('showcase.partials.js_scripts')
        </footer>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>