<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationSignalementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observation_signalement', function (Blueprint $table) {
            $table->increments('sig_id');
            $table->integer('reg_id');
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->string('sig_nom', 120);
            $table->string('sig_email', 120);
            $table->string('sig_numero_observation', 10);
            $table->text('sig_motif');
            $table->dateTime('sig_date');
            $table->text('sig_reponse_ce');
            $table->dateTime('sig_reponse_ce_date');
            $table->integer('reg_reponse_ce_int');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observation_signalement');
    }
}
