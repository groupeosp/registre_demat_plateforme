<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermanencesVisioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permanences_visio', function (Blueprint $table) {
            $table->increments('pev_id');
            $table->integer('reg_id');
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->dateTime('pev_debut');
            $table->dateTime('pev_fin');
            $table->string('pev_url', 120);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permanences_visio');
    }
}
