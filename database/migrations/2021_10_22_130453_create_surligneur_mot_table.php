<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurligneurMotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surligneur_mot', function (Blueprint $table) {
            $table->increments('sur_id');
            $table->integer('reg_id');
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->string('sur_mot', 60);
            $table->integer('sur_modif_int');
            $table->dateTime('sur_modif_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surligneur_mot');
    }
}
