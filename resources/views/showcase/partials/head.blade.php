<!-- Vue js form validation added token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/css/app.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Nunito Sans' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="resources\sass\showcase\head.scss" media="all"/>
<link rel="stylesheet" type="text/css" href="resources\sass\showcase\offers.scss" media="all"/>
<link rel="stylesheet" type="text/css" href="resources\sass\showcase\home.scss" media="all"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/LineIcons.2.0.css') }}">
<!-- Animate -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
<!-- Tailwind css -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/tailwind.css') }}">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('js/wow.js') }}"></script>
<script src="{{ asset('js/legal_mention.js') }}"></script>
    
<title>Registre Demat.</title>