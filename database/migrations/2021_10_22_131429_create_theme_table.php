<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theme', function (Blueprint $table) {
            $table->increments('the_id');
            $table->integer('reg_id');
            $table->foreign('reg_id')->references('reg_id')->on('registre')->onDelete('cascade');
            $table->string('the_nom', 255);
            $table->integer('the_modif_int');
            $table->dateTime('the_modif_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theme');
    }
}
