<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationPersoValeursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observation_perso_valeurs', function (Blueprint $table) {
            $table->integer('obs_id');
            $table->foreign('obs_id')->references('obs_id')->on('observation')->onDelete('cascade');
            $table->integer('opc_id');
            $table->foreign('opc_id')->references('opc_id')->on('observation_perso_champs')->onDelete('cascade');
            $table->text('obv_valeur1');
            $table->boolean('obv_valeur2');
            $table->boolean('obv_valeur3');
            $table->boolean('obv_valeur4');
            $table->boolean('obv_valeur5');
            $table->boolean('obv_valeur6');
            $table->boolean('obv_valeur7');
            $table->boolean('obv_valeur8');
            $table->boolean('obv_valeur9');
            $table->boolean('obv_valeur10');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observation_perso_valeurs');
    }
}
